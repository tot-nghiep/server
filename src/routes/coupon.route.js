const express = require("express");
const {
  getAllCoupon,
  getCouponById,
  addCoupon,
  updateCouponStatus,
  searchCoupon,
  checkCodeCoupon,
  filterCoupon,
} = require("../controllers/coupon.controller");
const {
  isValidDiscount,
  isValidDate,
  checkValidStatus,
} = require("../middlewares/couponValidation");
const checkRole = require("../middlewares/roleHandler");
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROLE } = require("../utils/constant");
const { validate } = require("../middlewares/validation");
const { ROUTES } = require("../utils/routes");

const router = express.Router();

//search coupon
router.get(
  ROUTES.SEARCH,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  searchCoupon
);

//check code coupon
router.get("/check-udc", checkCodeCoupon);

//filter copupon
router.get(
  ROUTES.FILTER,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  isValidDate,
  checkValidStatus,
  validate,
  filterCoupon
);

//get coupon by id
router.get(
  ROUTES.COUPON_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  getCouponById
);

//Add coupon
router.post(
  "/",
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  isValidDiscount,
  isValidDate,
  validate,
  addCoupon
);

//Edit coupon
router.put(
  ROUTES.COUPON_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  checkValidStatus,
  validate,
  updateCouponStatus
);

//get all coupon
router.get("/", verifyToken, checkRole([ROLE.OWNER, ROLE.ADMIN]), getAllCoupon);

module.exports = router;
