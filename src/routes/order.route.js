const express = require("express");
const router = express.Router();
const checkRole = require("../middlewares/roleHandler");
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROLE } = require("../utils/constant");
const {
  getAllOrder,
  createOrder,
  getOrderById,
  getOrderByUserId,
  searchOrder,
  changeOrderStatus,
  getTotalOrderByTime,
  getTotalRevenue,
  getTotalRevenueByProduct,
  assignOrderToWarehouse,
  filterOrder,
  cancelOrder,
} = require("../controllers/order.controller");
const { ROUTES } = require("../utils/routes");
const {
  checkValidStatus,
  checkValidProductQuantity,
  resolveDuplicateProduct,
  checkValidDate,
  isWarehouseUser,
} = require("../middlewares/orderValidation");
const { validate } = require("../middlewares/validation");

// get total Order by time
router.get(
  ROUTES.TOTAL_ORDER_BY_TIME,
  checkValidDate,
  validate,
  getTotalOrderByTime
);

// get total revenue
router.get(ROUTES.TOTAL_REVENUE, checkValidDate, validate, getTotalRevenue);

//get Total Revenue By Product
router.get(ROUTES.TOTAL_REVENUE_BY_PRODUCT, verifyToken, getTotalRevenueByProduct);

//Search order
router.get(ROUTES.SEARCH, verifyToken, searchOrder);

//Filter order
router.get(ROUTES.FILTER, verifyToken, filterOrder);

//get order by id
router.get(ROUTES.ORDER_BY_ID, verifyToken, getOrderById);

//get order by user id
router.get(ROUTES.ORDER_BY_USER_ID, verifyToken, getOrderByUserId);

//Create order
router.post(
  "/",
  verifyToken,
  resolveDuplicateProduct,
  checkValidProductQuantity,
  validate,
  createOrder
);

//assign to warehouse user
router.put(
  ROUTES.ASSIGN_ORDER,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isWarehouseUser,
  validate,
  assignOrderToWarehouse
);

//change order status
router.put(
  "/status/:orderId",
  verifyToken,
  checkRole([ROLE.ADMIN, ROLE.OWNER, ROLE.MOD]),
  checkValidStatus,
  validate,
  changeOrderStatus
);

router.put(
  ROUTES.CANCEL_ORDER,
  verifyToken,
  checkRole([ROLE.USER, ROLE.OWNER]),
  cancelOrder
);

//get all order
router.get(
  "/",
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD, ROLE.WAREHOUSE]),
  getAllOrder
);

//Search order

module.exports = router;
