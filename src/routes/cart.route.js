const express = require("express");
const router = express.Router();
const checkRole = require("../middlewares/roleHandler");
const { verifyToken } = require("../middlewares/tokenHandler");
const { isValidProduct,isProductExist } = require("../middlewares/productValidation")
const { ROLE } = require("../utils/constant");
const {validate} = require("../middlewares/validation")
const {
    getAllCart,
    addItem,
    deleteItem,
    updateItem,
} = require("../controllers/cart.controller");
const { ROUTES } = require("../utils/routes");

//delete item
router.delete(ROUTES.CART_DELETE_ITEM,
    verifyToken,
    isValidProduct,
    isProductExist,
    validate,
    deleteItem
    );

//update item
router.put(ROUTES.CART_UPDATE_ITEM,
    verifyToken,
    isValidProduct,
    isProductExist,
    validate,
    updateItem
    )

//add item
router.put("/",
    verifyToken,
    isValidProduct,
    isProductExist,
    validate,
    addItem);

//get all cart
router.get("/",
    verifyToken,
    getAllCart
);

module.exports = router;