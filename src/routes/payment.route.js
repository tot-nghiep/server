const express = require("express");
const { validate } = require("../middlewares/validation");
const router = express.Router();
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROUTES } = require("../utils/routes");
const {
  createPaymentUrl,
  paymentSuccess,
} = require("../controllers/payment.controller");

//create shipping order route
router.post(ROUTES.CREATE_PAYMENT_URL, createPaymentUrl);

router.get(ROUTES.PAYMENT_SUCCESS, paymentSuccess);

module.exports = router;
