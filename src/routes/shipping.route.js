const express = require("express");
const {
  createShippingOrder,
  cancelShippingOrder,
  detailShippingOrder,
  getAvailableServices,
  getFee,
  getProvince,
  getDistrict,
  getWard,
} = require("../controllers/shipping.controller");
const { validate } = require("../middlewares/validation");
const router = express.Router();
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROUTES } = require("../utils/routes");

//create shipping order route
router.post(ROUTES.CREATE, createShippingOrder);

//cancel shipping order route
router.post(ROUTES.CANCEL, cancelShippingOrder);

//detail shipping order route
router.get(ROUTES.DETAIL, detailShippingOrder);

//get available services
router.get(ROUTES.AVAILABLE_SERVICES, getAvailableServices);

//get fee
router.get(ROUTES.FEE, getFee);

//get province
router.get(ROUTES.PROVINCE, getProvince);

//get district
router.get(ROUTES.DISTRICT, getDistrict);

//get ward
router.get(ROUTES.WARD, getWard);

module.exports = router;
