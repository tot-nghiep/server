const express = require("express");
const {
  getAllUser,
  getUserById,
  editUser,
  getUserDetail,
  changeRoleAndStatus,
  searchUser,
  filterUser,
  getTotalUserByTime,
} = require("../controllers/user.controller");
const checkRole = require("../middlewares/roleHandler");
const { validate } = require("../middlewares/validation");
const { verifyToken } = require("../middlewares/tokenHandler");
const router = express.Router();
const { ROLE } = require("../utils/constant");
const {
  isValidUserId,
  checkValidUserStatus,
  checkValidUserRole,
  isMasterAdmin,
  checkValidDate,
} = require("../middlewares/userValidation");
const { ROUTES } = require("../utils/routes");

//get user detail
router.get(ROUTES.USER_DETAIL, verifyToken, getUserDetail);

//filter user
router.get(
  ROUTES.FILTER,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  checkValidUserRole,
  checkValidUserStatus,
  validate,
  filterUser
);

//get total users by time (create)
router.get(
  ROUTES.TOTAL_USER_BY_TIME,
  checkValidDate,
  validate,
  getTotalUserByTime
);

// search user
router.get(
  ROUTES.SEARCH,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  searchUser
);

// get user by id
router.get(
  ROUTES.USER_BY_ID,
  verifyToken,
  isValidUserId,
  isMasterAdmin,
  validate,
  getUserById
);

//Change user role and status
router.put(
  ROUTES.CHANGE_USER_ROLE_AND_STATUS,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN]),
  isValidUserId,
  isMasterAdmin,
  checkValidUserRole,
  checkValidUserStatus,
  validate,
  changeRoleAndStatus
);

//edit user
router.put(ROUTES.USER_BY_ID, verifyToken, isValidUserId, validate, editUser);

/*
 * get all user
 *  Add verify token with route require authentication
 *  Add checkrole with route require role.
 */
router.get("/", verifyToken, checkRole([ROLE.OWNER, ROLE.ADMIN]), getAllUser);

module.exports = router;
