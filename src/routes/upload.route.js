const express = require("express");
const cloudinary = require("cloudinary").v2;
const { upload, deleteImage } = require("../controllers/upload.controller");
const checkRole = require("../middlewares/roleHandler");
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROLE } = require("../utils/constant");
const router = express.Router();

//Upload product image
router.post(
  "/",
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  upload
);

//Delete product image
router.delete(
  "/",
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  deleteImage
);

module.exports = router;
