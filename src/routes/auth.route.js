const express = require("express");
const {
  login,
  register,
  resetPassword,
  changePassword,
  loginGoogle,
} = require("../controllers/auth.controller");
const { validate } = require("../middlewares/validation");
const router = express.Router();
const { body } = require("express-validator");
const User = require("../models/user.model");
const {
  isValidUsernameLength,
  isValidPasswordLength,
  isValidEmail,
  isUsernameExist,
  isMatchPasswordRegex,
  isConfirmPasswordMatch,
} = require("../middlewares/authValidation");
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROUTES } = require("../utils/routes");

//login route
router.post(
  ROUTES.LOGIN,
  isValidUsernameLength,
  isValidPasswordLength,
  validate,
  login
);

//register route
router.post(
  ROUTES.REGISTER,
  isValidUsernameLength,
  isUsernameExist,
  isValidPasswordLength,
  isMatchPasswordRegex,
  isConfirmPasswordMatch,
  isValidEmail,
  validate,
  register
);

router.post(ROUTES.GOOGLE_LOGIN, loginGoogle)
//Forgot password
router.post(ROUTES.FORGOT_PASSWORD, resetPassword);

//Change password
router.put(
  ROUTES.CHANGE_PASSWORD,
  verifyToken,
  isValidPasswordLength,
  isMatchPasswordRegex,
  isConfirmPasswordMatch,
  validate,
  changePassword
);

module.exports = router;
