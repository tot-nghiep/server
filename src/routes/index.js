const express = require("express");

const auth = require("./auth.route");
const user = require("./user.route");
const product = require("./product.route");
const category = require("./category.route");
const upload = require("./upload.route");
const order = require("./order.route");
const cart = require("./cart.route");
const coupon = require("./coupon.route");
const shipping = require("./shipping.route");
const payment = require("./payment.route");
const { ROUTES } = require("../utils/routes");

function router(app) {
  app.use(ROUTES.AUTH, auth);
  app.use(ROUTES.CATEGORY, category);
  app.use(ROUTES.COUPON, coupon);
  app.use(ROUTES.CART, cart);
  app.use(ROUTES.ORDER, order);
  app.use(ROUTES.PRODUCT, product);
  app.use(ROUTES.UPLOAD, upload);
  app.use(ROUTES.USER, user);
  app.use(ROUTES.SHIPPING, shipping);
  app.use(ROUTES.PAYMENT, payment);
}

module.exports = router;
