const express = require("express");
const checkRole = require("../middlewares/roleHandler");
const { validate } = require("../middlewares/validation");
const { verifyToken } = require("../middlewares/tokenHandler");
const { ROLE } = require("../utils/constant");
const {
  getAllCategory,
  updateCategory,
  getCategoryById,
  createSubcategory,
  createCategory,
  updateSubcategory,
  deleteSubcategory,
  deleteCategory,
} = require("../controllers/category.controller");
const {
  isValidCategoryId,
  isUncategory,
  isValidSubcategoryId,
  isCategoryExist,
  isUnsubcategory,
} = require("../middlewares/categoryValidation");
const { ROUTES } = require("../utils/routes");

const router = express.Router();

//Delete category
router.delete(
  ROUTES.CATEGORY_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isValidCategoryId,
  isUncategory,
  validate,
  deleteCategory
);

//Delete subcategory
router.delete(
  ROUTES.SUBCATEGORY_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isValidSubcategoryId,
  isUnsubcategory,
  deleteSubcategory
);

//Add category
router.post(
  "/",
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  createCategory
);

//Add subcategory
router.post(
  ROUTES.SUBCATEGORY,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isValidCategoryId,
  isCategoryExist,
  validate,
  createSubcategory
);

//Edit category
router.put(
  ROUTES.CATEGORY_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isValidCategoryId,
  isUncategory,
  validate,
  updateCategory
);

//Edit subcategory
router.put(
  ROUTES.SUBCATEGORY_BY_ID,
  verifyToken,
  checkRole([ROLE.OWNER, ROLE.ADMIN, ROLE.MOD]),
  isValidSubcategoryId,
  isUnsubcategory,
  validate,
  updateSubcategory
);

// get category by id
router.get(ROUTES.CATEGORY_BY_ID, isValidCategoryId, validate, getCategoryById);

//get all category
router.get("/", getAllCategory);

module.exports = router;
