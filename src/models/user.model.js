const mongoose = require("mongoose");
// const bcrypt = require("bcryptjs");
const validator = require("validator");

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minLength: 8,
    },
    name: {
      type: String,
    },
    address: {
      province: {
        provinceId: Number,
        provinceName: String,
      },
      district: {
        districtId: Number,
        districtName: String,
      },
      ward: {
        wardId: Number,
        wardName: String,
      },
      detail: String,
    },
    phone: {
      type: Number,
      // required: true,
      unique: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      // unique: true,
      trim: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Email invalid!");
        }
      },
    },
    gender: {
      type: String,
      default: "none",
      lowercase: true,
    },
    avatar: {
      type: String,
      default: "none",
    },
    role: {
      type: String,
      default: "user", //warehouse, mod, admin, user, owner
      lowercase: true,
    },
    status: {
      type: String,
      default: "active",
      lowercase: true,
    },
    signup:{
      type: String,
      default: "default",
      lowercase: true,
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);
