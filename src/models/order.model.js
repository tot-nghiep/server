const mongoose = require("mongoose");
const nanoid = require("nanoid");

const generateId = () => {
  const id = nanoid.customAlphabet("1234567890abcdefghijklmnopqrstuvwxyz", 10);
  return id();
};

const orderSchema = new mongoose.Schema(
  {
    code: {
      type: String,
      default: generateId,
    },
    shippingOrderCode: {
      type: String,
    },
    serviceTypeId:{
      type: Number,
    },
    warehouseUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    products: [
      {
        product: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Product",
        },
        quantity: {
          type: Number,
          default: 1,
        },
        currentPrice: {
          type: Number,
        },
      },
    ],
    coupon: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Coupon",
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    status: {
      type: String,
      default: "pending", //pending, assigned, picking, shipping, returning, returned, finished, cancel
      lowercase: true,
    },
    shippingDetail: {
      fullname: {
        type: String,
        required: true,
      },
      address: {
        province: {
          provinceId: Number,
          provinceName: String,
        },
        district: {
          districtId: Number,
          districtName: String,
        },
        ward: {
          wardId: String,
          wardName: String,
        },
        detail: String,
      },
      phone: {
        type: String,
        required: true,
      },
      email: {
        type: String,
      },
    },
    paymentStatus: {
      type: String,
      default: "unpaid", //paid, unpaid (Da thanh toan, chua thanh toan)
    },
    paymentType: {
      type: String,
    },
    totalPrice:{
      type: Number,
    },
    transactionNo: {
      type: Number,
    },
    payDate: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Order", orderSchema);
