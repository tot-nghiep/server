const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    code: {
      // ma san pham
      type: String,
      required: true,
      unique: true,
    },
    images: [
      {
        url: { type: String },
        public_id: {
          type: String,
        },
      },
    ],
    price: {
      type: Number,
      required: true,
    },
    available: {
      type: Number,
      // default: 1,
      required: true,
    },
    description: {
      type: String,
    },
    tags: {
      type: Array,
      lowercase: true,
    },
    brand: {
      type: String,
    },
    selling: {
      type: Number,
      default: 0,
    },
    subcategory: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Subcategory",
    },
    status: {
      type: String,
      default: "active",
      lowercase: true,
    },
    color:[
      {
        type: String,
      }
    ],
    discount:{
      type: Number
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Product", productSchema);
