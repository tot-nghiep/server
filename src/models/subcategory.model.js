const mongoose = require("mongoose");

const subcategorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  description:{
    type: String,
  },
  image: 
    {
      url: { type: String },
      public_id: {
        type: String,
      },
    },
});

module.exports = mongoose.model("Subcategory", subcategorySchema);
