require("dotenv").config();
const express = require("express");
// const logger = require("morgan");
const cors = require("cors");
const path = require("path");
const db = require("./config/db/index.js");
const route = require("./routes");
const port = process.env.PORT || 3001;
const cloudinary = require("cloudinary");
const fileUpload = require("express-fileupload");

const app = express();

db.connect();
app.use(cors());
// app.use(logger("dev"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(
  fileUpload({
    useTempFiles: true,
    limits: {
      fileSize: 50 * 1024 * 1024,
    },
    abortOnLimit: true,
  })
);

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET,
});

// const bodyParser = require('body-parser')

// app.use(bodyParser.json())
// app.use(bodyParser.urlencoded({ extended: true }))

app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

route(app);

app.listen(port, () =>
  console.log(`Server listening at http://localhost:${port}`)
);
