const { check } = require("express-validator");
const { UNCATEGORY_ID, UNSUBCATEGORY_ID } = require("../utils/constant");
const { isValidId } = require("./validation");
const Category = require("../models/category.model");

exports.isValidCategoryId = [
  check("categoryId").custom((value) => {
    if (!isValidId(value)) {
      return Promise.reject("Invalid category id");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isCategoryExist = [
  check("categoryId").custom(async (value) => {
    const category = await Category.findById(value);
    if (!category) {
      return Promise.reject("Category not exists");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isUncategory = [
  check("categoryId").custom((value) => {
    if (value === UNCATEGORY_ID) {
      return Promise.reject("Can't edit/delete this category");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isUnsubcategory = [
  check("subcategoryId").custom((value) => {
    if (value === UNSUBCATEGORY_ID) {
      return Promise.reject("Can't edit/delete this sub category");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isValidSubcategoryId = [
  check("subcategoryId").custom((value) => {
    if (!isValidId(value)) {
      return Promise.reject("Invalid subcategory id");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];
