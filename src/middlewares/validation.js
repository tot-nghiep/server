const { validationResult } = require("express-validator");
const {
  HTTP_STATUS,
  ROLE,
  USER_STATUS,
  REVIEW_SCORE,
  PRODUCT_STATUS,
  STATUS,
  ORDER_STATUS,
  DATE,
} = require("../utils/constant");
const mongoose = require("mongoose");

const listValidRole = Object.values(ROLE);
const listValidStatus = Object.values(STATUS);
const listValidOrderStatus = Object.values(ORDER_STATUS);
const listValidDate = Object.values(DATE);


exports.validate = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(HTTP_STATUS.BAD_REQUEST).json({
      success: false,
      status: 400,
      message: errors.array()[0].msg,
    });
  }
  next();
};

exports.isValidId = (value) => mongoose.Types.ObjectId.isValid(value);

exports.isValidRole = (value) => listValidRole.includes(value);

exports.isValidDate = (value) => listValidDate.includes(value);

exports.isValidStatus = (value) => listValidStatus.includes(value);

exports.isValidOrderStatus = (value) => listValidOrderStatus.includes(value);

exports.isValidReviewScore = (value) => REVIEW_SCORE.includes(value);
