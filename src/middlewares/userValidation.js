const { check } = require("express-validator");
const { UNCATEGORY_ID } = require("../utils/constant");
const {
  isValidId,
  isValidRole,
  isValidStatus,
  isValidDate,
} = require("./validation");
const User = require("../models/user.model");

exports.isValidUserId = [
  check("userId").custom((value) => {
    if (!isValidId(value)) {
      return Promise.reject("Invalid user id");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.checkValidUserStatus = [
  check("status").custom((value) => {
    if (value !== undefined) {
      if (!isValidStatus(value)) {
        return Promise.reject("Invalid status");
      }
    }
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.checkValidUserRole = [
  check("role").custom((value) => {
    if (value !== undefined) {
      if (!isValidRole(value)) {
        return Promise.reject("Invalid role");
      }
    }
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isMasterAdmin = [
  check("userId").custom((value) => {
    const masterAdminId = process.env.MASTER_ADMIN_ID;

    if (value === masterAdminId) {
      return Promise.reject("Can't get/edit this user");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.checkValidDate = [
  check("limit").custom((value) => {
    if (!isValidDate(value)) {
      return Promise.reject("Invalid Date");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];
