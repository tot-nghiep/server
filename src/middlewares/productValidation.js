const { check } = require("express-validator");
const { UNCATEGORY_ID, UNSUBCATEGORY_ID } = require("../utils/constant");
const { isValidId, isValidDate } = require("./validation");
const Product = require("../models/product.model");
const Subcategory = require("../models/subcategory.model");

exports.isValidProductId = [
  check("productId").custom((value) => {
    if (!isValidId(value)) {
      return Promise.reject("Invalid product id");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isValidProduct = [
  check("product").custom((value) => {
    if (!isValidId(value)) {
      return Promise.reject("Invalid product id");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isValidPrice = [
  check("price").custom((value) => {
    if (value !== undefined) {
      if (typeof value !== "number") {
        return Promise.reject("Price must be a number");
      }
      if (value <= 0) {
        return Promise.reject("Price must be greater than 0");
      } else return Promise.resolve();
    }
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isSubCategoryExist = [
  check("subcategory").custom(async (value) => {
    if (value !== undefined) {
      const subcategory = await Subcategory.findById(value);
      if (!subcategory) {
        return Promise.reject("Subcategory not exists");
      } else return Promise.resolve();
    }
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isProductExist = [
  check("product").custom(async (value) => {

    if (value !== undefined) {
      const product = await Product.findOne({_id: value});

      if (!product) {
        return Promise.reject("Product not exists");
      } else return Promise.resolve();
    }
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isValidAvailable = [
  check("available")
    .optional()
    .custom((value) => {
      if (typeof value !== "number") {
        return Promise.reject("Invalid available value");
      }
      if (value < 0) {
        return Promise.reject("Invalid available value");
      } else return Promise.resolve();
    }),
  (req, res, next) => {
    next();
  },
];

exports.checkValidDate = [
  check("limit").custom((value) => {
    if (!isValidDate(value)) {
      return Promise.reject("Invalid Date");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];
