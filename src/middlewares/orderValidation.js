const { isValidOrderStatus, isValidDate } = require("./validation");
const { check } = require("express-validator");
const User = require("../models/user.model");

exports.checkValidStatus = [
  check("status").custom((value) => {
    if (!isValidOrderStatus(value)) {
      return Promise.reject("Invalid order status");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.checkValidProductQuantity = [
  check("products.*.quantity").custom((value) => {
    if (value <= 0) {
      return Promise.reject("Product quantity must be greater than 0");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.resolveDuplicateProduct = (req, res, next) => {
  const { products } = req.body;
  const result = products.reduce((acc, e) => {
    const found = acc.find((x) => e.product === x.product);
    found ? (found.quantity += e.quantity) : acc.push(e);
    return acc;
  }, []);

  req.body.products = result;

  next();
};

exports.checkValidDate = [
  check("limit").custom((value) => {
    if (!isValidDate(value)) {
      return Promise.reject("Invalid Date");
    } else return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];

exports.isWarehouseUser = [
  check("warehouseUserId").custom(async (value) => {
    console.log(value);
    const user = await User.findById(value);
    if (!user) return Promise.reject("User not found");
    if (user.role !== "warehouse")
      return Promise.reject("User is not warehouse user");
    return Promise.resolve();
  }),
  (req, res, next) => {
    next();
  },
];
