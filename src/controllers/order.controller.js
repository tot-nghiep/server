const mongoose = require("mongoose");
const moment = require("moment");

const Order = require("../models/order.model");
const Coupon = require("../models/coupon.model");
const User = require("../models/user.model");
const Product = require("../models/product.model");
const Cart = require("../models/cart.model");

const { tokenDecode } = require("../middlewares/tokenHandler");
const {
  sendWarning,
  sendError,
  sendConflict,
  sendNotFound,
} = require("../utils/response");
const { ROUTES } = require("../utils/routes");
const { default: axios } = require("axios");
const { HTTP_STATUS, ROLE, ORDER_STATUS } = require("../utils/constant");
const product = require("../models/product.model");
const { checkCodeCoupon, checkCode } = require("./coupon.controller");

const BASE_URL = process.env.GHN_API_BASEURL;
const TOKEN = process.env.GHN_TOKEN;
const SHOP_ID = process.env.GHN_SHOP_ID;

//[GET] /getAllOrder
const getAllOrder = async (req, res) => {
  const { page, limit } = Object.assign({}, req.query);
  try {
    const orders = await Order.find()
      .populate("products.product")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();

    const totalRows = await Order.countDocuments();

    if (orders) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        totalRows,
        orders,
      });
    } else {
      sendWarning(res, "Get all order failed");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /getOrderById
const getOrderById = async (req, res) => {
  try {
    const order = await Order.findById(req.params.orderId)
      .populate("products.product")
      .populate("coupon", "discount");
    if (
      req.user.role === ROLE.OWNER ||
      req.user.role === ROLE.ADMIN ||
      req.user.role === ROLE.MOD ||
      req.user.role === ROLE.WAREHOUSE ||
      req.user._id.toString() === order.user.toString()
    ) {
      if (order) {
        res.status(HTTP_STATUS.SUCCESS).json({
          success: true,
          status: 200,
          order,
        });
      } else {
        sendWarning(res, "Order not found!");
      }
    } else return sendWarning(res, "You can't access this resource");
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /getTotalOrderByTime
const getTotalOrderByTime = async (req, res) => {
  const { limit } = Object.assign({}, req.query);
  try {
    let query = {
      createdAt: {
        $lte: moment().toDate(),
        $gte: moment().subtract(1, limit).toDate(),
      },
    };
    const total = await Order.find(query).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      total,
    });
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /getTotalRevenue
const getTotalRevenue = async (req, res) => {
  const { limit } = Object.assign({}, req.query);
  try {
    const result = await Order.aggregate([
      {
        $match: {
          createdAt: {
            $lte: moment().toDate(),
            $gte: moment().subtract(1, limit).toDate(),
          },
        },
      },
      {
        $unwind: "$products",
      },
      {
        $lookup: {
          from: "products",
          localField: "products.product",
          foreignField: "_id",
          as: "product",
        },
      },
      {
        $unwind: "$product",
      },
      {
        $group: {
          _id: null,
          totalRevenue: {
            $sum: {
              $multiply: ["$products.quantity", "$product.price"],
            },
          },
        },
      },
    ]);

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRevenue: result[0].totalRevenue,
    });
  } catch (error) {
    sendError(res, error);
  }
};
//[GET] /getTotalRevenueByProduct
const getTotalRevenueByProduct = async (req, res) => {

  try {
    const result = await Order.aggregate([
      {
        $unwind: "$products",
      },
      // Pha 2: Liên kết order với product để có thông tin chi tiết của sản phẩm
      {
        $lookup: {
          from: "products", // Tên của collection chứa thông tin product
          localField: "products.product",
          foreignField: "_id",
          as: "productInfo",
        },
      },
      // Pha 3: Giải mã các productInfo để có thông tin chi tiết của sản phẩm
      {
        $unwind: "$productInfo",
      },
      // Pha 4: Nhóm theo sản phẩm và tính tổng doanh thu cho mỗi sản phẩm
      {
        $group: {
          _id: "$productInfo._id",
          name: { $first: "$productInfo.name" },
          price: { $first: "$productInfo.price" },
          code: { $first: "$productInfo.code" },
          status: { $first: "$productInfo.status" },
          images: { $push: "$productInfo.images" },
          sold: { $sum:  "$products.quantity"},
          totalRevenue: { $sum: { $multiply: ["$products.quantity", "$products.currentPrice"] } },
        },
      },
      // Pha 5: Sắp xếp theo tổng doanh thu giảm dần
      {
        $sort: { totalRevenue: -1 },
      },
    ]);
    
    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRevenue: result,
    });
  } catch (error) {
    sendError(res, error);
  }
};


//[GET] /getOrderByUserId
const getOrderByUserId = async (req, res) => {
  const { userId } = req.params;
  try {
    if (
      req.user.role === ROLE.OWNER ||
      req.user.role === ROLE.ADMIN ||
      req.user.role === ROLE.MOD ||
      req.user._id.toString() === userId
    ) {
      const orders = await Order.find({ user: userId })
        .populate("products.product")
        .populate("user", "username email phone")
        .lean();

      if (!orders) return sendWarning(res, "Get orders failed!");

      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        orders,
      });
    } else return sendWarning(res, "You can't access this resource");
  } catch (error) {
    sendError(res, error);
  }
};

//[POST] /createOrder
const createOrder = async (req, res) => {
  let userId;
  const { products, coupon, shippingDetail,paymentType, serviceTypeId } = req.body;
  let { fullname, address, phone, email } = shippingDetail;
  try {
    let subTotal=0;
    for (let i = 0; i < products.length; i++) {
      const { product, quantity } = products[i];
      const productAvailable = await Product.findById(product).select(
        "available price discount"
      );

      if (quantity > productAvailable.available) {
        return res.status(HTTP_STATUS.BAD_REQUEST).json({
          success: false,
          status: 400,
          message: "Product quantity exceeds available!",
        });
      }
      subTotal += quantity * productAvailable.price *(100-productAvailable.discount)/100 ;
      products[i]["currentPrice"] = productAvailable.price;
    }

    //nếu người dùng đăng nhập thì lấy thông tin từ bảng user
    const tokenDecoded = await tokenDecode(req);
    if (tokenDecoded) {
      userId = tokenDecoded.id;
    }

    let totalPrice=subTotal;
    const data = {};
    data["products"] = products;
    data["shippingDetail"] = {
      fullname,
      address,
      phone,
      email,
    };
    data["paymentType"] = paymentType;
    if(!serviceTypeId) return sendWarning(res, "not found serviceTypeId ")
    data["serviceTypeId"] = serviceTypeId;

    if (userId) data["user"] = userId;

    // nếu có coupon thì check có hợp lệ k
    if (coupon) {
      const result = await checkCode(coupon);
      //Neu hop le thi tru so luong
      if (typeof result !== "string") {
        const currentCoupon = await toggleCouponQuantity(coupon, -1);

        totalPrice= subTotal* (100-currentCoupon.discount)/100;

        //Them coupon vao data
        data["coupon"] = currentCoupon._id;
      } else return sendWarning(res, "coupon k hop le");
    }


    data["totalPrice"]= totalPrice;
    // trừ số lượng sản phẩm từ Product
    toggleProductQuantity(products, "decrease");

    const newOder = await Order.create(data);

    if (!newOder) {
      //nếu tạo mới k thành công thì trả lại số lượng về sp
      toggleProductQuantity(products, "increase");

      //nếu tạo mới k thành công va co coupon thì trả lại số lượng coupon
      //Da check tinh hop le cua coupon o tren nen k check nua
      toggleCouponQuantity(coupon, 1);
      
      return sendWarning(res,"createOrder failure!")
    }
    //xóa cart khi tạo order thành công
    await Cart.findOneAndUpdate(
      {user: userId},
      {
        $set: {
          products: []
        }
      }
    );

    //kiểm tra phương thức thanh toán
    if( paymentType ==="online"){
      const response = await axios.post( 
        `${process.env.BASE_URL}/payment/create_payment_url`,
        {
          amount: totalPrice,
          orderDescription: `ecomx_${newOder._id}`,
          orderCode: newOder.code
        }
      );
      return res.json({
        createOrders: "success",
        payment: response.data
      });
    }

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
    });
  } catch (error) {
    sendError(res, error);
  }
};

//[PUT] /cancelOrder
const cancelOrder = async(req,res) => {
  const { orderId } = Object.assign({}, req.params);
  let userId;
  try {
    
    const order = await Order.findById(orderId).lean();
    if(!order) return sendNotFound(res, "not found Order")

    //nếu người dùng đăng nhập thì lấy thông tin từ bảng user
    const tokenDecoded = await tokenDecode(req);
    if (tokenDecoded) {
      userId = tokenDecoded.id;
    }

    if(order.user.toString() !== userId) sendNotFound(res, "not found Orders!")

    if(!(order.status === "pending" ||
      order.status ==="picking" ||
      order.status=== "assigned"))
      return sendWarning(res, "Order is in irrevocable status!")

    if(order.status === "picking"){

      let order_codes =order.shippingOrderCode;
      
      console.log(`${BASE_URL}${ROUTES.CANCEL}`);
      const response = await axios.post(
        `${BASE_URL}${ROUTES.CANCEL}`,
        {
          order_codes: [order_codes],
        },
        {
          headers: {
            Token: TOKEN,
            ShopId: SHOP_ID,
          },
        }
      );
 
      if(response.data.code !== 200||!response )
       return sendError(res, response)
  
    }
    
    await Order.findByIdAndUpdate(
      orderId,
      {
        $set:{
          status: "cancel"
        }
      })
    res.status(HTTP_STATUS.SUCCESS).json({
      status: 200,
      message: " cancel shipping order success !",
    });

  } catch (error) {
    return sendError(res,error)
  }
}

//[GET] /searchOrder
const searchOrder = async (req, res) => {
  const { name, code, phone, page, limit } = Object.assign({}, req.query);
  const { user } = req;

  var regex = {
    "shippingDetail.fullname": {
      $regex: name ? name : "",
      $options: "i",
    },
    code: {
      $regex: code ? code : "",
      $options: "i",
    },
    "shippingDetail.phone": {
      $regex: phone ? phone : "",
      $options: "i",
    },
  };

  if (user.role === ROLE.USER) {
    regex["user"] = user._id;
  }

  try {
    const orders = await Order.find(regex)
      .populate("products.product")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();
    const totalRows = await Order.find(regex).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRows,
      orders,
    });
  } catch (error) {
    sendError(res, error);
  }
};

const changeOrderStatus = async (req, res) => {
  const { orderId } = req.params;
  const { status } = Object.assign({}, req.body);
  try {
    //Check Order exist
    const currentOrder = await Order.findById(orderId);
    if (currentOrder.length === 0)
      return sendWarning(res, "Order is not exist");
    if (status === "pendding")
      return sendWarning(res, "cannot be changed to pending!");
    if (status === "completed" && currentOrder.status === "completed")
      return sendConflict(res, "status already exists!");
    const updatedOrder = await Order.findByIdAndUpdate(
      orderId,
      {
        $set: {
          status: status,
        },
      },
      { new: true }
    );
    if (!updatedOrder) return sendWarning(res, "Update Order status failed");

    //tăng selling của product khi status -> completed
    if (status === "completed") {
      currentOrder.products.forEach(async (element) => {
        const { product, quantity } = element;
        await Product.findByIdAndUpdate(product, {
          $inc: {
            selling: quantity,
          },
        });
      });
    }
    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      OrderStatus: updatedOrder.status,
    });
  } catch (error) {
    sendError(res, error);
  }
};

//Asign order to warehouse user
const assignOrderToWarehouse = async (req, res) => {
  const { warehouseUserId, orderId } = req.body;
  try {
    const currentOrder = await Order.findById(orderId);
    if (!currentOrder) return sendWarning(res, "Order not found");

    //khi status cua order la shipping, returning ....thi khong cho assign
    if (currentOrder.status !== ORDER_STATUS.PENDING)
      return sendWarning(res, "You cant assign order to warehouse right now");
    
    if (currentOrder.paymentType === "online" && currentOrder.paymentStatus === "unpaid")
      return sendWarning(res, "You cant assign order to warehouse right now");

    const updatedOrder = await Order.findByIdAndUpdate(
      orderId,
      {
        $set: {
          warehouseUser: warehouseUserId,
          status: ORDER_STATUS.ASSIGNED,
        },
      },
      { new: true }
    );

    if (!updatedOrder) return sendWarning(res, "Assign warehouse user failed");

    //tăng selling của product khi status -> completed
    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      order: updatedOrder,
    });
  } catch (error) {
    sendError(res, error);
  }
};

const filterOrder = async (req, res) => {
  const { page, limit, status, paymentStatus, warehouseUser } = Object.assign(
    {},
    req.query
  );

  var query = {};
  if (status !== undefined) query["status"] = status;
  if (paymentStatus !== undefined) query["paymentStatus"] = paymentStatus;
  if (warehouseUser !== undefined) query["warehouseUser"] = warehouseUser;

  try {
    const orders = await Order.find(query)
      .populate("warehouseUser", "_id username")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();
    const totalRows = await Order.find(query).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRows,
      orders,
    });
  } catch (error) {
    sendError(res, error);
  }
};

const toggleCouponQuantity = async (code, quantity) => {
  const coupon = await Coupon.findOneAndUpdate(
    { code: code },
    {
      $inc: {
        available: quantity,
      },
    },
    { new: true }
  );
  return coupon;
};

const toggleProductQuantity = async (products, type) => {
  products.forEach(async (element) => {
    const { product, quantity } = element;
    const updateProduct = await Product.findByIdAndUpdate(product,
      {
        $inc: {
          available: type === "decrease" ? -quantity : quantity,
        }
      },
      {new: true}
    );
  });
};

module.exports = {
  getAllOrder,
  getOrderById,
  getOrderByUserId,
  createOrder,
  searchOrder,
  changeOrderStatus,
  getTotalOrderByTime,
  getTotalRevenue,
  getTotalRevenueByProduct,
  assignOrderToWarehouse,
  filterOrder,
  cancelOrder,
};
