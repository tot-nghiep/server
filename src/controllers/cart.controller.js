const mongoose = require("mongoose");

const Cart = require("../models/cart.model");
const User = require("../models/user.model");
const Product = require("../models/product.model");

const { tokenDecode } = require("../middlewares/tokenHandler");
const {
  sendWarning,
  sendError,
  sendConflict,
  sendNotFound,
} = require("../utils/response");

const { HTTP_STATUS, ROLE, ORDER_STATUS } = require("../utils/constant");

//[GET] /getAllCart
const getAllCart = async (req, res) => {

    let userId;
    try {
        const tokenDecoded = await tokenDecode(req);
        if (tokenDecoded) {
          userId = tokenDecoded.id;   
        }

        const cart = await Cart.findOne({ user: userId})
            .populate("products.product")
            .sort({ createAt: -1})
            .lean();

        if (!cart)  sendWarning(res, "Get all order failed");

        res.status(HTTP_STATUS.SUCCESS).json({
            success: true,
            status: 200,
            cart,
          });
    } catch (error) {
        sendError(res, error);
    }
}

const addItem = async (req, res) => {
    let userId;
    const { product, quantity} = req.body;
    try {

        if(quantity <=0 ) return sendWarning(res,"quantity unable <= 0")

        const tokenDecoded = await tokenDecode(req);
        if (tokenDecoded) {
          userId = tokenDecoded.id;   
        }
        
        let newCart;
        
        // kiểm tra xem trong cart đã có sp này chưa
        const checkProduct = await Cart.findOne({ 
            user: userId,
            "products.product": product,
        });

        if(checkProduct){
            newCart = await Cart.findOneAndUpdate(
                {user: userId,"products.product": product },
                {
                    $inc: { "products.$.quantity": quantity }
                },
                { new : true}
            ).populate("products.product")
        }else {
            newCart = await Cart.findOneAndUpdate(
                {user: userId},
                {
                    $push: {
                        products: { product: product, quantity: quantity }
                    }
                },
                { new : true}
            ).populate("products.product")
        }

        // // kiểm tra sau khi update nếu quantity <=0 xóa product trong cart
        // if (newCart.products.find((item) => item.quantity <= 0)) {
        //     newCart= await deleteItem(userId, product);
        //     return  res.status(HTTP_STATUS.SUCCESS).json({
        //             success: true,
        //             status: 200,
        //             newCart,
        //         });;
        // }

        res.status(HTTP_STATUS.SUCCESS).json({
            success: true,
            status: 200,
            newCart,
          });
    
    } catch (error) {
        sendError(res, error)
    }
}

const deleteItem = async (req, res) => {
    const { product } = req.body;
    let userId;
    try {

        const tokenDecoded = await tokenDecode(req);
        if (tokenDecoded) {
          userId = tokenDecoded.id;   
        }

        // kiểm tra xem trong cart đã có sp này chưa
        const checkProduct = await Cart.findOne({ 
            user: userId,
            "products.product": product,
        });
        if(!checkProduct) return sendWarning(res, "Item not exist!!")

        const newCart = await Cart.findOneAndUpdate(
            { user: userId },
            { $pull: { products: { product: product } } },
            { new: true }
        ).populate("products.product");

        if( !newCart) return sendError(res, "delete Item not success!")

        res.status(HTTP_STATUS.SUCCESS).json({
            success: true,
            status: 200,
            newCart,
        });
    } catch (error) {
        sendError(res, error)
    }
};

const updateItem = async (req, res) => {
    const {product, quantity} = req.body;
    let userId;
    try {

        const tokenDecoded = await tokenDecode(req);
        if (tokenDecoded) {
          userId = tokenDecoded.id;   
        }

        newCart = await Cart.findOneAndUpdate(
            {user: userId, "products.product": product},
            {
                $set: { "products.$.quantity": quantity }
            },
            { new : true}
        ).populate("products.product")

        if( !newCart) return sendError(res, "update Item not success!")

        res.status(HTTP_STATUS.SUCCESS).json({
            success: true,
            status: 200,
            newCart,
        });
        
    } catch (error) {
        sendError(res, error)
    }
}

module.exports = {
    getAllCart,
    addItem,
    deleteItem,
    updateItem,
}