const { sendError } = require("../utils/response");
const { HTTP_STATUS } = require("../utils/constant");
const axios = require("axios");
const { ROUTES } = require("../utils/routes");
const Order = require("../models/order.model");

const createPaymentUrl = async (req, res, next) => {
  var ipAddr =
    req.headers["x-forwarded-for"] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;

  var config = require("config");
  var dateFormat = require("dateformat");
  var dateFns = require("date-fns");

  var tmnCode = config.get("vnp_TmnCode");
  var secretKey = config.get("vnp_HashSecret");
  var vnpUrl = config.get("vnp_Url");
  var returnUrl = config.get("vnp_ReturnUrl");

  var date = new Date();


  var createDate = dateFns.format(date, "yyyyMMddHHmmss");
  var orderCode = req.body.orderCode;
  var amount = req.body.amount;

  var orderInfo = req.body.orderDescription;
  var orderType = 1000000;

  var returnUrls = process.env.BASE_URL + returnUrl

  var currCode = "VND";
  var vnp_Params = {};
  vnp_Params["vnp_Version"] = "2.1.0";
  vnp_Params["vnp_Command"] = "pay";
  vnp_Params["vnp_TmnCode"] = tmnCode;
  // vnp_Params['vnp_Merchant'] = ''
  vnp_Params["vnp_Locale"] = "vn";
  vnp_Params["vnp_CurrCode"] = currCode;
  vnp_Params["vnp_TxnRef"] = orderCode;
  vnp_Params["vnp_OrderInfo"] = orderInfo;
  vnp_Params["vnp_OrderType"] = orderType;
  vnp_Params["vnp_Amount"] = amount * 100;
  vnp_Params["vnp_ReturnUrl"] = returnUrls;
  vnp_Params["vnp_IpAddr"] = ipAddr;
  vnp_Params["vnp_CreateDate"] = createDate;

  vnp_Params = sortObject(vnp_Params);

  var querystring = require("qs");
  var signData = querystring.stringify(vnp_Params, { encode: false });
  var crypto = require("crypto");
  var hmac = crypto.createHmac("sha512", secretKey);
  var signed = hmac.update(new Buffer(signData, "utf-8")).digest("hex");
  vnp_Params["vnp_SecureHash"] = signed;
  vnpUrl += "?" + querystring.stringify(vnp_Params, { encode: false });

  // res.redirect(vnpUrl);

  res.status(200).json({
    msg: "success",
    url: vnpUrl,
  });
};

const paymentSuccess = async (req, res) => {

  //Cap nhat DB
  const data = req.query;

  const { vnp_OrderInfo, vnp_ResponseCode } = data;
  const orderId = vnp_OrderInfo.split("_")[1];
  try {
    if( vnp_ResponseCode !== "00"){
      return res.redirect(`https://e-commerce-itisxoi.vercel.app/status/${orderId}`)
    }

    await Order.findByIdAndUpdate( orderId,
      {
        $set: { paymentStatus: "paid"}
      }
      )

    return res.redirect(`https://e-commerce-itisxoi.vercel.app/status/${orderId}`)

  } catch (error) {
    sendError(res, error)
  }

};

function sortObject(obj) {
  let sorted = {};
  let str = [];
  let key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) {
      str.push(encodeURIComponent(key));
    }
  }
  str.sort();
  for (key = 0; key < str.length; key++) {
    sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
  }
  return sorted;
}

module.exports = { createPaymentUrl, paymentSuccess };
