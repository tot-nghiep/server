const Product = require("../models/product.model");
const User = require("../models/user.model");
const moment = require("moment");
const {
  sendWarning,
  sendError,
  sendConflict,
  sendNotFound,
} = require("../utils/response");

const { HTTP_STATUS, ROLE } = require("../utils/constant");
const { tokenDecode } = require("../middlewares/tokenHandler");

//[GET] /getAllProduct for admin
const getAllProductAdmin = async (req, res) => {
  const { page, limit, status } = Object.assign({}, req.query);
  try {
    var products;
    if (status === undefined) {
      products = await Product.find()
        .populate("subcategory")
        .limit(limit)
        .skip((page - 1) * limit)
        .sort({ createdAt: -1 })
        .lean();
    } else {
      products = await Product.find({ status: status })
        .populate("subcategory")
        .limit(limit)
        .skip((page - 1) * limit)
        .sort({ createdAt: -1 })
        .lean();
    }

    const totalRows = await Product.countDocuments();
    if (products) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        totalRows,
        products,
      });
    } else {
      sendWarning(res, "Get all product failed");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] get total product by time
const getTotalProductByTime = async (req, res) => {
  const { limit } = Object.assign({}, req.query);
  try {
    let query = {
      createdAt: {
        $lte: moment().toDate(),
        $gte: moment().subtract(1, limit).toDate(),
      },
    };
    const total = await Product.find(query).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      total,
    });
  } catch (error) {
    sendError(res, error);
  }
};
// Get all product for client
const getAllProduct = async (req, res) => {
  const { page, limit } = Object.assign({}, req.query);
  try {
    const products = await Product.find({ status: "active" })
      .populate("subcategory")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();

    const totalRows = await Product.countDocuments({ status: "active" });
    if (products) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        totalRows,
        products,
      });
    } else {
      sendWarning(res, "Get all product failed");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /:productId
const getProductById = async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId).populate(
      "subcategory"
    );
    if (product) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        product,
      });
    } else {
      sendWarning(res, "Product not found");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /subcategory/:subcategoryId
const  getProductBySubcategory = async (req, res) => {
  const { subcategoryId } = req.params;
  try {
    const products = await Product.find({
      subcategory: subcategoryId,
    }).populate("subcategory");

    if (products) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        products,
      });
    } else {
      sendWarning(res, "Product not found");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /subcategory/brand/:subcategoryId
const  getBrandBySubcategory = async (req, res) => {
  const { subcategoryId } = req.params;
  try {
    const brands = await Product.find({
      subcategory: subcategoryId,
    }).distinct("brand");

    if (brands) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        brands,
      });
    } else {
      sendWarning(res, "Product not found");
    }
  } catch (error) {
    sendError(res, error);
  }
};
// [GET] /newest
const getNewestProduct = async (req, res) => {
  const { page, limit } = Object.assign({}, req.query);
  try {
    const products = await Product.find()
      .populate("subcategory")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();
    if (products) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        totalRows: 20,
        products,
      });
    } else {
      sendWarning(res, "Get newest product failed");
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /bestSelling
const getBestSellingProduct = async (req, res) => {
  const { limit } = Object.assign({}, req.query);
  try {
    const products = await Product.find({status: "active"})
      .populate("subcategory")
      .limit(limit)
      .sort({ selling: -1 })
      .lean();
    if (products) {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        products,
      });
    } else sendWarning(res, "Get best selling product failed!");
  } catch (error) {
    sendError(res, error);
  }
};
// [POST] /create
const createProduct = async (req, res) => {
  const {
    name,
    code,
    images,
    price,
    available,
    description,
    tags,
    brand,
    subcategory,
    color,
    discount
  } = req.body;

  const checkCode = await Product.findOne({ code: code }).lean();
  try {
    if (checkCode) return sendConflict(res, "code of Product already exist");
    const newProduct = await Product.create({
      name,
      code,
      images,
      price,
      available,
      description,
      tags,
      brand,
      subcategory,
      color,
      discount
    });
    if (!newProduct) return sendWarning(res, "Create Product failed!");

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      newProduct,
    });
  } catch (error) {
    sendError(res, error);
  }
};

//[PUT] /:productId/update
const updateProduct = async (req, res) => {
  const { productId } = req.params;
  const {
    name,
    images,
    price,
    available,
    description,
    tags,
    brand,
    subcategory,
    color,
    discount
  } = req.body;

  try {
    //Check product exist
    const checkProductId = await Product.findById(productId);
    if (checkProductId.length === 0)
      return sendNotFound(res, "ProductId not exist");

    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      {
        $set: {
          name,
          images,
          price,
          available,
          description,
          tags,
          brand,
          subcategory,
          color,
          discount
        },
      },
      { new: true }
    ).populate("subcategory");

    if (!updateProduct) {
      sendWarning(res, "Update product failed");
    } else {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        product: updatedProduct,
      });
    }
  } catch (error) {
    sendError(res, error);
  }
};

const changeProductStatus = async (req, res) => {
  const { productId } = req.params;

  try {
    //Check product exist
    const currentProduct = await Product.findById(productId);

    if (currentProduct.length === 0)
      return sendWarning(res, "Product is not exist");

    const currentStatus = currentProduct.status;

    const updatedProduct = await Product.findByIdAndUpdate(
      productId,
      {
        $set: {
          status: currentStatus === "active" ? "inactive" : "active",
        },
      },
      { new: true }
    );
    if (!updatedProduct)
      return sendWarning(res, "Update product status failed");
    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      productStatus: updatedProduct.status,
    });
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /check-upc
const checkCodeProduct = async (req, res) => {
  const { code } = req.query;
  try {
    const check = await Product.findOne({ code: code }).lean();

    if (check) return sendConflict(res, "code of Product already exist!!");
    else {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        message: "code of product usable.",
      });
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /brand Get all brand
const getAllBrand = async (req, res) => {
  try {
    const brands = await Product.distinct("brand");

    if (!brands) return sendWarning(res, "Get all warning failed");
    else {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        brands,
      });
    }
  } catch (error) {
    sendError(res, error);
  }
};

//[GET] /brand/check Check brand exist
const checkBrandExist = async (req, res) => {
  const { brand } = req.body;
  try {
    const isBrandExist = await Product.find({ brand: brand.toLowerCase() });

    if (isBrandExist.length !== 0)
      return sendWarning(res, "Brand name already exist");
    else {
      res.status(HTTP_STATUS.SUCCESS).json({
        success: true,
        status: 200,
        message: "Brand name is useable",
      });
    }
  } catch (error) {
    sendError(res, error);
  }
};

const searchProduct = async (req, res) => {
  const { name, code, page, limit } = Object.assign({}, req.query);

  var regex = {
    name: {
      $regex: name ? name : "",
      $options: "i",
    },
    code: {
      $regex: code ? code : "",
      $options: "i",
    },
  };

  //if user not logged in or user role is not admin or mod, return active product only
  const tokenDecoded = await tokenDecode(req);
  if (tokenDecoded) {
    const userId = tokenDecoded.id;
    const user = await User.findById(userId).select("username role");
    //check user role is user
    if (user.role === "user") regex["status"] = "active";
  }

  try {
    const products = await Product.find(regex)
      .populate("subcategory")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();
    const totalRows = await Product.find(regex).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRows,
      products,
    });
  } catch (error) {
    sendError(res, error);
  }
};

const filterProduct = async (req, res) => {
  const { page, limit, subCategoryId, brand,color, discount,minPrice,maxPrice  } = Object.assign({}, req.query);
  // const { subCategoryId, brand, discount, rating } = Object.assign(
  //   {},
  //   req.body
  // );
  var query = {};
  if (subCategoryId !== undefined) query["subcategory"] = subCategoryId;
  if (brand !== undefined && brand.length > 0) query["brand"] = { $in: brand };
  if (color !== undefined ) query["color"]= { $in: color };
  if (discount !== undefined) query["discount"] = { $lt: discount };
  if (minPrice !== undefined && maxPrice === undefined) query["price"] = { $gte: minPrice };
  if (minPrice !== undefined && maxPrice !== undefined) query["price"] = { $gte: minPrice, $lte: maxPrice};
  if (minPrice === undefined && maxPrice !== undefined) query["price"] = { $lte: maxPrice };

  //if user not logged in or user role is not admin or mod, return active product only
  const tokenDecoded = await tokenDecode(req);
  if (tokenDecoded) {
    const userId = tokenDecoded.id;
    const user = await User.findById(userId).select("username role");
    //check user role is user
    if (user.role === "user") query["status"] = "active";
  } 

  try {
    const products = await Product.find(query)
      .populate("subcategory")
      .limit(limit)
      .skip((page - 1) * limit)
      .sort({ createdAt: -1 })
      .lean();
    const totalRows = await Product.find(query).count();

    res.status(HTTP_STATUS.SUCCESS).json({
      success: true,
      status: 200,
      totalRows,
      products,
    });
  } catch (error) {
    sendError(res, error);
  }
};

module.exports = {
  getAllProduct,
  getAllProductAdmin,
  getProductById,
  getNewestProduct,
  updateProduct,
  createProduct,
  checkCodeProduct,
  changeProductStatus,
  getProductBySubcategory,
  getAllBrand,
  checkBrandExist,
  searchProduct,
  filterProduct,
  getBestSellingProduct,
  getTotalProductByTime,
  getBrandBySubcategory,
};
