exports.HTTP_STATUS = {
  SUCCESS: 200,
  BAD_REQUEST: 400,
  NOT_FOUND: 404,
  UNAUTHENTICATED: 401,
  UNAUTHORIZED: 403,
  GEN_UNPROCESSABLE_ENTITY: 422,
  INTERNAL_SERVER_ERROR: 500,
  SERVICE_TEMPORARILY_OVERLOADED: 502,
  SERVICE_UNAVAILABLE: 503,
  CONFLICT: 409,
  TOO_MANY_REQUEST: 429,
};

exports.ROLE = {
  OWNER: "owner",
  ADMIN: "admin",
  MOD: "mod",
  WAREHOUSE: "warehouse",
  USER: "user",
};

exports.STATUS = {
  ACTIVE: "active",
  INACTIVE: "inactive",
};

exports.ORDER_STATUS = {
  PENDING: "pending",
  COMPLETED: "completed",
};

exports.DATE = {
  DAYS: "days",
  WEEKS: "weeks",
  MONTHS: "months",
};

exports.ORDER_STATUS = {
  PENDING: "pending",
  ASSIGNED: "assigned",
  PICKING: "picking",
  SHIPPING: "shipping",
  RETURNING: "returning",
  RETURNED: "returned",
  FINISHED: "finished",
  CANCEL: "cancel",
};

exports.REVIEW_SCORE = [1, 2, 3, 4, 5];

//Production
exports.UNCATEGORY_ID = "63fecafc2dbbd8fd9d9b5395";
exports.UNSUBCATEGORY_ID = "63fecb022dbbd8fd9d9b539d";

exports.CLOUDINARY_PRODUCT_FOLDER = "products";
exports.CLOUDINARY_AVATAR_FOLDER = "avatar";

exports.setResetPassEmailContent = (username, newPassword) => {
  return `Your new password for user ${username} in Ecomx system is: \n ${newPassword} \n If you did not request to reset your password, it is safe to disregard this message. You can learn more about why you may have received this email here.\n
  Best regards, \n
  The Ecomx Team`;
};

exports.randomPassword = (length) => {
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+";
  let result = "";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};
