exports.ROUTES = {
  //Search
  SEARCH: "/search",
  FILTER: "/filter",

  //Auth
  AUTH: "/auth",
  LOGIN: "/login",
  REGISTER: "/register",
  GOOGLE_LOGIN: "/google-login",
  FORGOT_PASSWORD: "/forgot",
  CHANGE_PASSWORD: "/password/change",

  //Category
  CATEGORY: "/category",
  CATEGORY_BY_ID: "/:categoryId",
  SUBCATEGORY: "/subcategory",
  SUBCATEGORY_BY_ID: "/subcategory/:subcategoryId",

  //Coupon
  COUPON: "/coupon",
  COUPON_BY_ID: "/:couponId",

  //Cart
  CART: "/cart",
  CART_UPDATE_ITEM: "/update",
  CART_DELETE_ITEM: "/delete",

  //Order
  ORDER: "/order",
  ORDER_BY_ID: "/:orderId",
  TOTAL_REVENUE: "/revenue",
  TOTAL_REVENUE_BY_PRODUCT: "/revenue-by-product",
  TOTAL_ORDER_BY_TIME: "/total",
  ORDER_BY_USER_ID: "/user/:userId",
  CANCEL_ORDER: "/cancel/:orderId",
  ASSIGN_ORDER: "/assign",

  //Product
  PRODUCT: "/product",
  PRODUCT_ADMIN: "/admin",
  PRODUCT_BY_ID: "/:productId",
  PRODUCT_BY_SUBCATEGORY: "/subcategory/:subcategoryId",
  TOTAL_PRODUCT_TIME: "/total",
  BRAND: "/brand",
  BRAND_BY_SUBCATEGORY: "/subcategory/brand/:subcategoryId",
  CHECK_BRAND: "/brand/check",
  NEWEST_PRODUCT: "/newest",
  BEST_SELLING_PRODUCT: "/best-selling",
  CHANGE_PRODUCT_STATUS: "/status/:productId",
  CHECK_PRODUCT_CODE: "/check-upc",

  //Upload
  UPLOAD: "/upload",

  //User
  USER: "/user",
  TOTAL_USER_BY_TIME: "/total",
  USER_BY_ID: "/:userId",
  USER_DETAIL: "/detail",
  CHANGE_USER_ROLE_AND_STATUS: "/change/:userId",

  //Shipping
  SHIPPING: "/shipping",
  CREATE: "/shipping-order/create",
  DETAIL: "/shipping-order/detail",
  CANCEL: "/switch-status/cancel",
  AVAILABLE_SERVICES: "/shipping-order/available-services",
  FEE: "/shipping-order/fee",
  PROVINCE: "/master-data/province",
  DISTRICT: "/master-data/district",
  WARD: "/master-data/ward",

  //Payment
  PAYMENT: "/payment",
  CREATE_PAYMENT_URL: "/create_payment_url",
  PAYMENT_SUCCESS: "/success",
};
